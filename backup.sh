#!/bin/bash
USERNAME=youruser
PASSWORD=yourpassword
INSTANCE=example.atlassian.net
LOCATION=/where/to/store/the/file

# Set this to your Atlassian instance's timezone.
# See this for a list of possible values:
# https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
TIMEZONE=America/Los_Angeles

# Setting this to "true", will make the script compress the backup
# file with the xz utility. The backup file downloaded
# from Jira, is already a compressed zip file, so the difference 
# in file size will be small (my 637MB zip file is reduced to a 632MB file). 
# On the other hand, it will require a lot of memory and cpu to do this.
XZ_COMPRESS_FILE="false"
 
# Grabs cookies and generates the backup on the UI. 
TODAY=`TZ=$TIMEZONE date +%Y%m%d`
COOKIE_FILE_LOCATION=jiracookie
curl --silent -u $USERNAME:$PASSWORD --cookie-jar $COOKIE_FILE_LOCATION https://${INSTANCE}/Dashboard.jspa --output /dev/null
BKPMSG=`curl -s --cookie $COOKIE_FILE_LOCATION --header "X-Atlassian-Token: no-check" -H "X-Requested-With: XMLHttpRequest" -H "Content-Type: application/json"  -X POST https://${INSTANCE}/rest/obm/1.0/runbackup -d '{"cbAttachments":"true" }' `
rm $COOKIE_FILE_LOCATION
 
#Checks if the backup procedure has failed
if [ `echo $BKPMSG | grep -i backup | wc -l` -ne 0 ]; then
	#The $BKPMSG variable will print the error message, you can use it if you're planning on sending an email
	exit 2
fi

FILE_ZIP_NAME="JIRA-backup-${TODAY}.zip"
 
#Checks if the backup exists in WebDAV every 10 seconds, 20 times. If you have a bigger instance with a larger backup file you'll probably want to increase that.
for (( c=1; c<=20; c++ )); do
	wget --user=$USERNAME --password=$PASSWORD --spider https://${INSTANCE}/webdav/backupmanager/$FILE_ZIP_NAME >/dev/null 2>/dev/null && OK=0 || OK=$?
	if [ $OK -eq 0 ]; then
		break
	fi
	sleep 10
done
 
#If after 20 attempts it still fails it ends the script.
if [ $OK -ne 0 ]; then
	echo "Ending the script, as the backup does not seem to become available."
	exit 1
else 
	#If it's confirmed that the backup exists on WebDAV the file get's copied to the $LOCATION directory.
	wget --user=$USERNAME --password=$PASSWORD -t 0 --retry-connrefused https://${INSTANCE}/webdav/backupmanager/$FILE_ZIP_NAME -P "$LOCATION" >/dev/null 2>/dev/null
fi

if [ $XZ_COMPRESS_FILE = "true" ]; then
	xz -9e --check=sha256 -T 0 $FILE_ZIP_NAME
fi
